package com.example.credit

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.example.eventbus.EventBusModel
import org.example.eventbus.MessageEvent

class CreditActivity : AppCompatActivity() {

    lateinit var amountEditText: TextInputEditText
    lateinit var amountInputLayout: TextInputLayout
    lateinit var datePickerButton: Button
    private lateinit var textView:TextView
    var date: Long = 0
    var amount:Double = -100000.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val arguments = intent.extras
        val name = arguments!!["amount"].toString()
        setContentView(R.layout.activity_credit)
        amount = name.toDouble()

        textView = findViewById(R.id.amountCreditTextView)
        handleEvent(MessageEvent.MessageAmount(amount))

        amountEditText = findViewById(R.id.amountInputEditText)
        amountInputLayout = findViewById(R.id.amountTextInputLayout)
        datePickerButton = findViewById(R.id.datePickerButton)
        val picker = MaterialDatePicker.Builder.datePicker()
            .setTitleText("Select date")
            .build()
        picker.addOnPositiveButtonClickListener {
            date = it
        }
        picker.addOnNegativeButtonClickListener {
            date = 0
        }
        picker.addOnCancelListener {
            date = 0
        }
        picker.addOnDismissListener {
            date = 0
        }
        datePickerButton.setOnClickListener {
            picker.show(supportFragmentManager, "MaterialDatePicker")
        }
        findViewById<Button>(R.id.takeCreditButton).setOnClickListener {
            val amountAdd = amountEditText.text.toString().trim()

            if (amountAdd.isEmpty() || amountAdd.toDouble() <= 0.0) {
                amountInputLayout.error = "Amount must be greater than zero!"
                amountInputLayout.requestFocus()
                return@setOnClickListener
            }

            val intent = Intent()
            intent.setClassName(this, "com.example.appbank.MainActivity")
            intent.putExtra("amount", (amount+amountAdd.toDouble()).toString())
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)

            lifecycleScope.launch {
                EventBusModel.produceEventSus(MessageEvent.MessageAmount(amount+amountAdd.toDouble()))
            }
        }
        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.RESUMED) {
                launch {
                    EventBusModel.events.collect {
                        withContext(Dispatchers.Main) {
                            handleEvent(it)
                        }
                    }
                }
            }
        }
    }

    private fun handleEvent(event: MessageEvent) = when (event) {
        is MessageEvent.MessageAmount -> {
            amount = event.amount
            textView.text = "У тебя есть $amount"
        }
        else -> {}
    }


    override fun onDestroy() {
        lifecycleScope.launch {
            EventBusModel.produceEventSus(MessageEvent.MessageAmount(amount))
        }
        super.onDestroy()

        lifecycleScope.launch {
            EventBusModel.produceEventSus(MessageEvent.MessageAmount(amount))
        }
    }
}